# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# Maintained by Keegan for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel
# RebornOS Discord: Keegan

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
import time
import json
from pathlib import Path
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("RebornOS FIRE")
try:
    import httplib
except:
    import http.client as httplib

# Create variables for both the current working directory and the location of the settings file
workingDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
localeDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'TranslationFiles'))
settingsFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Settings', 'settings.json'))
packageFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Settings', 'packages.txt'))
runFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Settings', 'neverRun.txt'))
startupFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Bash', 'StartupChecks.sh'))
bashFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Bash', 'Maintenance.sh'))
gladeFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Glade', 'FIRE.glade'))
UnnecessaryFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Python', 'RebornUnnecessary.py'))
SelectFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Python', 'RebornSelect.py'))
RemoveFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Python', 'RebornRemove.py'))
DowngradeFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Python', 'RebornDowngrade.py'))
SettingsFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Python', 'RebornSettings.py'))
MycroftDesktopFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Mycroft', 'mycroft.desktop'))

installApricity = 'bash ' + workingDirectory + '/reborn-updates Apricity'
installPantheon = 'bash ' + workingDirectory + '/reborn-updates Pantheon'

# Update files that hold the lists of LigthDM and SDDM greeters + themes currently on the system, as well as the potential rollback opportunities
os.system('bash ' + startupFile + ' FirstTime ' + runFile + ' ' + packageFile + ' ' + SettingsFile + ' ' + workingDirectory)
os.system('bash ' + startupFile + ' DisplayLightDMgreeters ' + workingDirectory)
os.system('bash ' + startupFile + ' DisplaySDDMgreeters ' + workingDirectory)
os.system('bash ' + startupFile + ' StorePacmanLog ' + workingDirectory)

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. Some options may not work").show()

# Create Handlers (Triggers) for each item
class Handler:
    def __init__(self):
        self.listoptions1 = [0,0,0,0,0]
        self.listoptions2 = [0,0,0,0,0]

# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### Top Menu ######################################
################################################################################

# Launch the settings window
    def RebornSettings(self, button):
        os.system('python3 ' + SettingsFile)

################################################################################
############################### First Tab ######################################
################################################################################

# Test a LightDM Greeter
    def LightDMTestGreeter(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            chosenGreeter = model[tree_iter][0]
            os.system('bash ' + bashFile + ' TestLightdm ' + chosenGreeter)

# Set LightDM as default
    def LightDMdefault(self, button):
        os.system('pkexec bash ' + bashFile + ' LightDMdefault')
        Notify.Notification.new("LightDM set as default").show()

# Set SDDM as default
    def SDDMdefault(self, button):
        os.system('pkexec bash ' + bashFile + ' SDDMdefault')
        Notify.Notification.new("SDDM set as default").show()

# Test LightDM Greeter
    def LightDMTestGreeter(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            chosenGreeter = model[tree_iter][0]
            os.system('bash ' + bashFile + ' LightDMdefault ' + chosenGreeter)

# Set LightDM Greeter
    def LightDMUseGreeter(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            chosenGreeter = model[tree_iter][0]
            os.system('pkexec bash ' + bashFile + ' LightDM ' + chosenGreeter)
            Notify.Notification.new("LightDM greeter changed").show()

# Test SDDM Greeter
    def SDDMTestGreeter(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            chosenGreeter = model[tree_iter][0]
            os.system('sddm-greeter --test-mode --theme /usr/share/sddm/themes/' + chosenGreeter)

# Set SDDM Greeter
    def SDDMUseGreeter(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            chosenGreeter = model[tree_iter][0]
            os.system('pkexec bash ' + bashFile + ' SDDM ' + chosenGreeter)
            Notify.Notification.new("SDDM greeter changed").show()

################################################################################
############################### Second Tab ######################################
################################################################################

# Install Apricity
    def clickApricity(self, button):
        subprocess.Popen(['xterm', '-e', installApricity])

    # Install Budgie
    def clickBudgie(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment budgie ' + settingsFile)

    # Install Cinnamon
    def clickCinnamon(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment cinnamon ' + settingsFile)

    # Install Deepin
    def clickDeepin(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment deepin ' + settingsFile)

    # Install Enlightenment
    def clickEnlightenment(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment enlightenment ' + settingsFile)

    # Install GNOME
    def clickGnome(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment gnome ' + settingsFile)

    # Install i3
    def clicki3(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment i3 ' + settingsFile)

    # Install Plasma
    def clickPlasma(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment plasma ' + settingsFile)

    # Install LXQt
    def clickLXQt(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment lxqt ' + settingsFile)

    # Install Mate
    def clickMate(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment mate ' + settingsFile)

    # Install Openbox
    def clickOpenbox(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment openbox ' + settingsFile)

    # Install Pantheon
    def clickPantheon(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment pantheon ' + settingsFile)

    # Install XFCE
    def clickXFCE(self, button):
        os.system('bash ' + bashFile + ' DesktopEnvironment xfce ' + settingsFile)

################################################################################
############################### Third Tab ######################################
################################################################################

    # Clear Pacman's cache completely
    def onClearCache(self, switch, state):
        if state == True:
            self.listoptions1[0]=1
        else:
            self.listoptions1[0]=0

    # Clean the system journal excepting the last 3 days worth of text
    def onCleanJournal(self, switch, state):
        if state == True:
            self.listoptions1[1]=1
        else:
            self.listoptions1[1]=0

    # Clean package list
    def onCleanPackages(self, switch, state):
        if state == True:
            self.listoptions1[2]=1
        else:
            self.listoptions1[2]=0

    # Rank the mirrors
    def onRankMirrors(self, switch, state):
        if state == True:
            self.listoptions1[3]=1
        else:
            self.listoptions1[3]=0

    # Allow the user to remove programs that are no longer necessary - orphans, depreciated, etc
    def onUnnecessaryPrograms(self, switch, state):
        if state == True:
            self.listoptions1[4]=1
        else:
            self.listoptions1[4]=0

    # Apply select changes from above
    def clickApply1(self, button):
        print(self.listoptions1)
        if self.listoptions1[0] == 1:
            print("Clearing Cache (leaving nothing behind)...")
            os.system('yes| pkexec pacman -Scc')
            Notify.Notification.new("Cache Cleared (leaving nothing behind)").show()

        if self.listoptions1[1] == 1:
            print("Cleaning Journal...")
            os.system('pkexec journalctl --vacuum-time=3d && sync')
            Notify.Notification.new("Journal Cleaned").show()

        if self.listoptions1[2] == 1:
            print("Cleaning Package Cache (leaving the 3 most recent package versions)...")
            os.system('pkexec paccache -ruk0')
            Notify.Notification.new("Package Cache Cleaned (leaving the 3 most recent package versions)").show()

        if self.listoptions1[3] == 1:
            print("Ranking Mirrors...")
            Notify.Notification.new("Ranking Mirrors...").show()
            os.system('pkexec reflector --verbose -p https --sort rate --save /etc/pacman.d/mirrorlist')
            Notify.Notification.new("Mirrors Ranked").show()

        if self.listoptions1[4] == 1:
            Notify.Notification.new("Generating Package List...").show()
            os.system('rm -f ' + workingDirectory + '/package-files.txt')
            os.system('pkexec pacman -Qdtq >' + workingDirectory + '/package-files.txt')
            os.system('python3 ' + UnnecessaryFile)

################################################################################
############################### Fourth Tab #####################################
################################################################################

    def onSaveRecoverPackages(self, switch, state):
        if state == True:
            self.listoptions2[0]=1
        else:
            self.listoptions2[0]=0

    def onRebuildGrub(self, switch, state):
        if state == True:
            self.listoptions2[1]=1
        else:
            self.listoptions2[1]=0

    def onReinstallGrubEFI(self, switch, state):
        if state == True:
            self.listoptions2[2]=1
        else:
            self.listoptions2[2]=0

    def onDowngrade(self, switch, state):
        if state == True:
            self.listoptions2[3]=1
        else:
            self.listoptions2[3]=0

    def onRemovePackage(self, switch, state):
        if state == True:
            self.listoptions2[4]=1
        else:
            self.listoptions2[4]=0

    def clickApply2(self, button):
        print(self.listoptions2)
        if self.listoptions2[0] == 1:
            print("Save/Recover Packages...")
            os.system('python3 ' + SelectFile)
            Notify.Notification.new("Operation Complete").show()

        if self.listoptions2[1] == 1:
            print("Reinstalling Grub...")
            Notify.Notification.new("Reinstalling Grub...").show()
            os.system('pkexec pacman -S grub --noconfirm')
            Notify.Notification.new("Grub has Been Reinstalled").show()

        if self.listoptions2[2] == 1:
            print("Rebuilding Grub (EFI)...")
            os.system('pkexec bash ' + bashFile + ' Grub')
            Notify.Notification.new("Grub is Rebuilt").show()

        if self.listoptions2[3] == 1:
            print("Downgrading interface started...")
            os.system('python3 ' + DowngradeFile)
            Notify.Notification.new("Package Downgraded").show()

        if self.listoptions2[4] == 1:
            print("Package Removal interface started...")
            os.system('python3 ' + RemoveFile)
            Notify.Notification.new("Package Removed").show()

################################################################################
############################### Fifth Tab ######################################
################################################################################

    def rollbackUse(self, pkgtxt):
        global enteredText
        enteredText = pkgtxt.get_text()
        os.system('pkexec bash ' + bashFile + ' Rollback ' + enteredText + ' ' + workingDirectory + '/transactions.txt ' + settingsFile)
        Notify.Notification.new("Rollback Complete. Please Reboot for Full Effect").show()

    def resetRollbackUse(self, button):
        os.system('bash ' + bashFile + ' ResetRollback ' + settingsFile)

################################################################################
############################### Sixth Tab ######################################
################################################################################

    # Control Mycroft
    def clickControlMycroft(self, button):
        # Check to see if Mycroft/s desktop file already is already in place
        mycroftFile = Path("/usr/share/applications/mycroft.desktop")
        if mycroftFile.exists():
            os.system('pkexec bash ' + bashFile + ' RemoveMycroft ' + settingsFile)
            # Inform the user by updating the button label
            builder.get_object("Mycroft").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["ControlMycroft"])
        else:
            os.system('pkexec cp ' + MycroftDesktopFile + ' /usr/share/applications/')
            os.system('python3 ' + workingDirectory + '/Mycroft/mycroft-reborn.py &')
            # Inform the user by updating the button label
            builder.get_object("Mycroft").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["RemoveMycroft"])

    # Control Anbox
    def clickControlAnbox(self, button):
        os.system('pkexec bash ' + bashFile + ' InstallAnbox')

    # Send File with Wormhole
    def wormholeFile(self, menuitem):
        global filechosen
        filechosen = menuitem.get_file().get_path()
        os.system('pkexec bash ' + bashFile + ' SetupWormhole ' + filechosen)

    # Send Folder with Wormhole
    def wormholeFolder(self, menuitem):
        global filechosen
        filechosen = menuitem.get_file().get_path()
        print(filechosen)
        os.system('pkexec bash ' + bashFile + " SetupWormhole '" + filechosen + "'")

    # Receive File / Folder with Wormhole
    def wormholeReceive(self, pkgtxt):
        global enteredText
        enteredText = pkgtxt.get_text()
        os.system('pkexec bash ' + bashFile + ' ReceiveWormhole ' + enteredText)

################################################################################
############################### Seventh Tab ######################################
################################################################################

    # Open Pace
    def clickPacman(self, button):
        os.system('bash ' + bashFile + ' InstallTools pace ' + settingsFile)

    # Open Arch Kernel Manager
    def clickKernel(self, button):
        os.system('bash ' + bashFile + ' InstallTools pyakm-manager ' + settingsFile)

    # Open Stacer
    def clickStacer(self, button):
        os.system('bash ' + bashFile + ' InstallTools stacer ' + settingsFile)

    # Open Pamac
    def clickPamac(self, button):
        os.system('bash ' + bashFile + ' InstallTools pamac-manager ' + settingsFile)

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(gladeFile)
builder.connect_signals(Handler())

# =====================================
# View the file when the window pops up
# =====================================
with open(workingDirectory + '/Settings/lightdm.txt', 'r') as lightdmFile:
    Lines = lightdmFile.readlines()
    for line in Lines:
        builder.get_object("LightDMTestGreeterBox").append_text(line.strip())
        builder.get_object("LightDMUseGreeterBox").append_text(line.strip())
# =====================================

# =====================================
# View the file when the window pops up
# =====================================
with open(workingDirectory + '/Settings/sddm.txt', 'r') as sddmFile:
    Lines = sddmFile.readlines()
    for line in Lines:
        builder.get_object("SDDMTestThemeBox").append_text(line.strip())
        builder.get_object("SDDMUseThemeBox").append_text(line.strip())
# =====================================

# =====================================
# View the file when the window pops up
# =====================================
with open(workingDirectory + '/Settings/transactions.txt', 'r') as rollbackFile:
    rollbackData = rollbackFile.read()
    builder.get_object("rollbackView").get_buffer().set_text(rollbackData)
# =====================================

# =====================================
# Set text through the use of keys from the JSON files used for translations
# =====================================
with open(localeDirectory + '/translations_' + os.getenv('LANG').split('_')[0] + '.json') as json_file:
    locale = json.load(json_file)

    # Fetch the main stack for the sidebar
    main_stack = builder.get_object("main_stack")

    # SETTINGS BUTTON
    builder.get_object("SettingsDialog").set_label(locale["RebornOSFIRE"]["Settings"]["SettingsDialog"])

    # REBORNOS CUSTOMIZATION MAIN TAB
    rebornos_customization_page = main_stack.get_child_by_name("rebornos_customization_page")
    main_stack.child_set_property(rebornos_customization_page, "title", locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["RebornOSCustomization"])
    # -----> Display Managers sub-Tab
    builder.get_object("DisplayManagers").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["DisplayManagers"])
    builder.get_object("LightDM").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["LightDMColumn"]["LightDM"])
    builder.get_object("LightDMdefault").set_label(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["LightDMColumn"]["DefaultDM"])
    builder.get_object("LightDMUseGreeterText").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["LightDMColumn"]["UseGreeter"])
    builder.get_object("LightDMTestGreeterText").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["LightDMColumn"]["TestGreeter"])
    builder.get_object("SDDM").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["SDDMColumn"]["SDDM"])
    builder.get_object("SDDMdefault").set_label(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["SDDMColumn"]["DefaultDM"])
    builder.get_object("SDDMUseThemeText").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["SDDMColumn"]["UseTheme"])
    builder.get_object("SDDMTestThemeText").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DisplayManagersTab"]["SDDMColumn"]["TestTheme"])
    # -----> Desktop Environments sub-Tab
    builder.get_object("DesktopEnvironments").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["DesktopEnvironments"])
    builder.get_object("Budgie").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["Budgie"])
    builder.get_object("Cinnamon").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["Cinnamon"])
    builder.get_object("Deepin").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["Deepin"])
    builder.get_object("Enlightenment").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["Enlightenment"])
    builder.get_object("GNOME").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["GNOME"])
    builder.get_object("i3").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["i3"])
    builder.get_object("KDEplasma").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["KDEplasma"])
    builder.get_object("LXQt").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["LXQt"])
    builder.get_object("Mate").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["Mate"])
    builder.get_object("OpenBox").set_text(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["OpenBox"])
    builder.get_object("XFCE").set_label(locale["RebornOSFIRE"]["RebornOSCustomizationTab"]["DesktopEnvironmentsTab"]["XFCE"])

    # REBORNOS SYSTEM TASKS TAB
    system_tasks_page = main_stack.get_child_by_name("system_tasks_page")
    main_stack.child_set_property(system_tasks_page, "title", locale["RebornOSFIRE"]["SystemTasksTab"]["SystemTasks"])
    # -----> System Maintenance sub-tab
    builder.get_object("SystemMaintenanceTab").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["SystemMaintenance"])
    builder.get_object("CacheRemove").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["CacheRemove"])
    builder.get_object("CacheLeave").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["CacheLeave"])
    builder.get_object("CleanJournal").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["CleanJournal"])
    builder.get_object("RankMirrors").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["RankMirrors"])
    builder.get_object("RemoveUnnecessaryPrograms").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["RemoveUnnecessaryPrograms"])
    builder.get_object("goApply3").set_label(locale["RebornOSFIRE"]["SystemTasksTab"]["SystemMaintenanceTab"]["Apply"])
    # -----> Repair sub-tab
    builder.get_object("RepairTab").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["Repair"])
    builder.get_object("SaveRecover").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["SaveRecover"])
    builder.get_object("ReinstallGrub").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["ReinstallGrub"])
    builder.get_object("RebuildGrub").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["RebuildGrub"])
    builder.get_object("Downgrade").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["Downgrade"])
    builder.get_object("RemovePackage").set_text(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["RemovePackage"])
    builder.get_object("goApply1").set_label(locale["RebornOSFIRE"]["SystemTasksTab"]["RepairTab"]["Apply"])

    # REBORNOS PROGRAMS TAB
    programs_page = main_stack.get_child_by_name("programs_page")
    main_stack.child_set_property(programs_page, "title", locale["RebornOSFIRE"]["ProgramsTab"]["Programs"])
    # -----> Addons sub-tab
    builder.get_object("AddonsTab").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["Addons"])
    # Checks if Anbox is installed or not
    #   1) Stores output of shell command determining if anbox is installed as a snap already or not
    #   2) Determines the appropriate text to display for Anbox based on the result of step 1
    isAnboxInstalled = subprocess.check_output("snap list | grep 'anbox' | wc -l", shell=True)
    if isAnboxInstalled is True:
        builder.get_object("Anbox").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["RemoveAnbox"])
    else:
        builder.get_object("Anbox").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["InstallAnbox"])
    # Checks is Mycroft is installed or not
    #   1) Saves mycroft.desktop file location in a variable
    #   2) Saves mycroft-core directory location in a variable
    #   3) Enacts condition of 1 and 2 in order to determine the appropriate text to display for Mycroft
    mycroftFile= Path("/usr/share/applications/mycroft.desktop")
    userHome = subprocess.check_output("grep $(who | awk 'FNR == 1 {print $1}') /etc/passwd | rev | cut -f2 -d : | rev", shell=True)
    userHome = userHome.decode()
    mycroftDirectory = Path(userHome + "/mycroft-core")
    if mycroftFile.exists() or mycroftDirectory.exists():
        builder.get_object("Mycroft").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["RemoveMycroft"])
    else:
        builder.get_object("Mycroft").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["AddonsTab"]["ControlMycroft"])
    # -----> Tools sub-tab
    builder.get_object("ToolsTab").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["ToolsTab"]["Tools"])
    builder.get_object("PacmanSettings").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["ToolsTab"]["PacmanSettings"])
    builder.get_object("ArchKernelManager").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["ToolsTab"]["ArchKernelManager"])
    builder.get_object("Stacer").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["ToolsTab"]["Stacer"])
    builder.get_object("Pamac").set_label(locale["RebornOSFIRE"]["ProgramsTab"]["ToolsTab"]["Pamac"])
    # -----> Wormhole sub-tab
    builder.get_object("WormholeTab").set_text(locale["RebornOSFIRE"]["ProgramsTab"]["WormholeTab"]["Wormhole"])
    builder.get_object("WhatIsWormhole").set_text(locale["RebornOSFIRE"]["ProgramsTab"]["WormholeTab"]["WhatIsWormhole"])
    builder.get_object("WormholeDescription").set_text(locale["RebornOSFIRE"]["ProgramsTab"]["WormholeTab"]["WormholeDescription"])
    builder.get_object("SelectFile").set_text(locale["RebornOSFIRE"]["ProgramsTab"]["WormholeTab"]["SelectFile"])
    builder.get_object("SelectFolder").set_text(locale["RebornOSFIRE"]["ProgramsTab"]["WormholeTab"]["SelectFolder"])
    builder.get_object("EnterCode").set_placeholder_text(locale["RebornOSFIRE"]["ProgramsTab"]["WormholeTab"]["EnterCode"])

    # REBORNOS ROLLBACK TAB
    rollback_page = main_stack.get_child_by_name("rollback_page")
    main_stack.child_set_property(rollback_page, "title", locale["RebornOSFIRE"]["RollbackTab"]["Rollback"])

    builder.get_object("RollbackSystem").set_text(locale["RebornOSFIRE"]["RollbackTab"]["RollbackSystem"])
    builder.get_object("InsertOptionNumber").set_placeholder_text(locale["RebornOSFIRE"]["RollbackTab"]["InsertOptionNumber"])
    builder.get_object("UpgradeToCurrent").set_label(locale["RebornOSFIRE"]["RollbackTab"]["UpgradeToCurrent"])
# =====================================

window2 = builder.get_object("RebornOSFIRE")
window2.show_all()

Gtk.main()

Notify.uninit()
